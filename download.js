const puppeteer = require('puppeteer');
const fs = require("fs")

function pausecomp(millis) {
 var date = new Date();
 var curDate = null;
 do { curDate = new Date(); }
 while (curDate - date < millis);
}

(async () => {

 const link = "https://wallpapercave.com/u/Kim_Jennie/"

 let db = fs.readFileSync('./db.json', { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 let links = db[link].links

 for (let i = 0; i < links.length; i++) {
  const browser = await puppeteer.launch({
   headless: false,
  });

  let page = (await browser.pages())[0]

  let db = fs.readFileSync('./db.json', { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  links = db[link].links

  try {
   await page.goto(links[links.length - 1], { waitUntil: 'networkidle2' });
  } catch (err) { }

  links.pop()
  pausecomp(2000)
  await browser.close();

  fs.writeFileSync("./db.json", JSON.stringify(db, null, 2))
 }

})();