const puppeteer = require('puppeteer');
const fs = require("fs")

function pausecomp(millis) {
 var date = new Date();
 var curDate = null;
 do { curDate = new Date(); }
 while (curDate - date < millis);
}

const fetchLinks = async (page) => {
 const wallpapersSelector = "#web .collectionc .wp-grid"
 await page.waitForSelector(wallpapersSelector)
 console.log("done")
 // console.log([...await page.$$eval(wallpapersSelector, ell => {
 //  return ell.map(e => e.querySelector('a').href)
 // })])
 const list = await page.evaluate(wallpapersSelector => {
  const wallps = [[document.querySelectorAll("#web .collectionc .wp-grid")][0][0].querySelectorAll("a")][0]
  const list = []
  Object.keys(wallps).forEach(a => {
   list.push(`https://wallpapercave.com/download/uwp-${wallps[a.toString()].id}`)
  })


  return list
 }, wallpapersSelector)
 return list
}

const checkFull = async (link) => {
 let db = fs.readFileSync('./db.json', { encoding: "utf-8", flag: "r" })
 db = await JSON.parse(db)
 return db[link].pages + 1 === db[link].actualPage ? false : true
}

const launch = async () => {
 const browser = await puppeteer.launch({
  headless: false
 });
 return browser
}

(async () => {

 const link = "https://wallpapercave.com/u/_SanaUnnie_/"

 while (await checkFull(link) === true) {
  const browser = await launch()
  let page = (await browser.pages())[0]
  let db = fs.readFileSync('./db.json', { encoding: "utf-8", flag: "r" })
  db = await JSON.parse(db)
  let actualPage = db[link].actualPage

  await page.goto(`${link}${actualPage}`, { waitUntil: 'networkidle2' });

  const links = await fetchLinks(page)

  links.forEach(image => {
   db[link].links.push(image)
  })

  db[link].actualPage++
  fs.writeFileSync("./db.json", JSON.stringify(db, null, 2))
  await page.close()
 }
})();